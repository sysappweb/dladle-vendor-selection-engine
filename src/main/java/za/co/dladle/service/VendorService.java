package za.co.dladle.service;

import org.springframework.stereotype.Service;
import za.co.dladle.model.Vendor;
import za.co.dladle.model.VendorRequest;
import za.co.dladle.model.VendorResponse;

import java.util.List;
import java.util.Collections;

/**
 * Created by prady on 7/1/2017.
 */
@Service
public class VendorService {
    public VendorResponse find(VendorRequest vendorRequest) {

        Vendor vendor = computeAndSortVendorRequest(vendorRequest, vendorRequest.isEmergency());

        // Prepare Vendor Response
        VendorResponse vendorResponse = new VendorResponse();
        vendorResponse.setVendorId(vendor.getVendorId());
        vendorResponse.setWeighted(vendor.getWeightedRating());
        return vendorResponse;
    }

    private Vendor computeAndSortVendorRequest(VendorRequest aVendorRequest, boolean aIsEmergency) {
        // Sanity check
        if (null == aVendorRequest) {
            return null;
        }
        List<Vendor> lVendorList = aVendorRequest.getVendors();
        if (null == lVendorList || lVendorList.isEmpty()) {
            return null;
        }

        // Compute Weight for all vendors.
        for (Vendor v : lVendorList) {
            v.computeVendorWeight(aIsEmergency);
            v.print();
        }
        Collections.sort(lVendorList);
        return lVendorList.get(0);
    }
}
