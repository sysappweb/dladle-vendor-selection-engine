package za.co.dladle.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import za.co.dladle.apiutil.ApiConstants;
import za.co.dladle.apiutil.DladleConstants;
import za.co.dladle.apiutil.ResponseUtil;
import za.co.dladle.model.VendorRequest;
import za.co.dladle.model.VendorResponse;
import za.co.dladle.service.VendorService;

import java.io.IOException;
import java.util.Map;

/**
 * Created by prady on 7/1/2017.
 */
@RestController
public class VendorRequestController {
    @Autowired
    private VendorService vendorService;

    //------------------------------------------------------------------------------------------------------------------
    //Find correct Vendor
    //------------------------------------------------------------------------------------------------------------------
    @RequestMapping(value = ApiConstants.VENDOR_REQUEST, method = RequestMethod.POST)
    public Map<String, Object> findCorrectVendor(@RequestBody VendorRequest vendorRequest) throws IOException {
        try {
            VendorResponse vendorResponse = vendorService.find(vendorRequest);
            return ResponseUtil.response(DladleConstants.SUCCESS_RESPONSE, vendorResponse, DladleConstants.VENDOR_REQUEST);
        } catch (Exception e) {
            return ResponseUtil.response(DladleConstants.FAILURE_RESPONSE, null, e.getMessage());
        }
    }

}
