package za.co.dladle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by Pradyumna on 6/13/2016.
 */
@SpringBootApplication
public class Sandbox {

    public static void main(String[] args) {
        SpringApplication.run(Sandbox.class);
    }

}
