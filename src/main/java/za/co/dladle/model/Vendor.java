package za.co.dladle.model;

/**
 * Created by prady on 7/1/2017.
 */
public class Vendor implements Comparable<Vendor> {

    // These values to be initialized.
    private Long vendorId;
    private Double experience;
    private Double proximity;
    private Double rating;
    private Double feeStartRange;
    private Double feeEndRange;
    private Long mRequestTime;

    // These values to be computed after initialization.
    private Double mWeightedRating;
    private Double mAverageWeight;

    public Vendor() {
    }

    public Long getVendorId() {
        return vendorId;
    }

    public void setVendorId(Long vendorId) {
        this.vendorId = vendorId;
    }

    public Double getExperience() {
        return experience;
    }

    public void setExperience(Double experience) {
        this.experience = experience;
    }

    public Double getProximity() {
        return proximity;
    }

    public void setProximity(Double proximity) {
        this.proximity = proximity;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public Double getFeeStartRange() {
        return feeStartRange;
    }

    public void setFeeStartRange(Double feeStartRange) {
        this.feeStartRange = feeStartRange;
    }

    public Double getFeeEndRange() {
        return feeEndRange;
    }

    public void setFeeEndRange(Double feeEndRange) {
        this.feeEndRange = feeEndRange;
    }

    private void setWeightedRating(Double aWeightedRating) {
        this.mWeightedRating = aWeightedRating;
    }

    public Double getWeightedRating() {
        return this.mWeightedRating;
    }

    private void setAverageWeight(Double aAverageWeight) {
        this.mAverageWeight = aAverageWeight;
    }

    public Long getRequestTime() {
        return mRequestTime;
    }

    public void setRequestTime(Long aTime) {
        this.mRequestTime = aTime;
    }

    public void computeVendorWeight(boolean isEmergency) {
        double lWeightedRating = 0.0;
        double lAverageWeight = 0.0;
        if (isEmergency) {
            // 1. Compute Job Fee difference
            double feeDiff = this.feeEndRange - this.feeStartRange;
            if (feeDiff < 200) {
                lWeightedRating += VendorConstants.PARAM_WEIGHT_EMERGENCY[VendorConstants.JOB_FEE_INDEX]
                                * VendorConstants.JOB_FEE_EMERGENCY[0];
                lAverageWeight += VendorConstants.JOB_FEE_EMERGENCY[0];
            } else if (feeDiff >= 200 && feeDiff < 500) {
                lWeightedRating += VendorConstants.PARAM_WEIGHT_EMERGENCY[VendorConstants.JOB_FEE_INDEX]
                        * VendorConstants.JOB_FEE_EMERGENCY[1];
                lAverageWeight += VendorConstants.JOB_FEE_EMERGENCY[1];
            } else {
                lWeightedRating += VendorConstants.PARAM_WEIGHT_EMERGENCY[VendorConstants.JOB_FEE_INDEX]
                        * VendorConstants.JOB_FEE_EMERGENCY[2];
                lAverageWeight += VendorConstants.JOB_FEE_EMERGENCY[2];
            }
            // 2. Proximity
            if (this.proximity < 15) {
                lWeightedRating += VendorConstants.PARAM_WEIGHT_EMERGENCY[VendorConstants.PROXIMITY_INDEX]
                                * VendorConstants.PROXIMITY_EMERGENCY[0];
                lAverageWeight += VendorConstants.PROXIMITY_EMERGENCY[0];
            } else if (this.proximity >= 15 && this.proximity < 30) {
                lWeightedRating += VendorConstants.PARAM_WEIGHT_EMERGENCY[VendorConstants.PROXIMITY_INDEX]
                        * VendorConstants.PROXIMITY_EMERGENCY[1];
                lAverageWeight += VendorConstants.PROXIMITY_EMERGENCY[1];
            } else {
                lWeightedRating += VendorConstants.PARAM_WEIGHT_EMERGENCY[VendorConstants.PROXIMITY_INDEX]
                        * VendorConstants.PROXIMITY_EMERGENCY[2];
                lAverageWeight += VendorConstants.PROXIMITY_EMERGENCY[2];
            }
            // 3. Rating
            if (this.rating <= 2) {
                lWeightedRating += VendorConstants.PARAM_WEIGHT_EMERGENCY[VendorConstants.RATING_INDEX]
                                * VendorConstants.RATING_EMERGENCY[0];
                lAverageWeight += VendorConstants.RATING_EMERGENCY[0];
            } else if (this.rating > 2 && this.rating < 4) {
                lWeightedRating += VendorConstants.PARAM_WEIGHT_EMERGENCY[VendorConstants.RATING_INDEX]
                        * VendorConstants.RATING_EMERGENCY[1];
                lAverageWeight += VendorConstants.RATING_EMERGENCY[1];
            } else {
                lWeightedRating += VendorConstants.PARAM_WEIGHT_EMERGENCY[VendorConstants.RATING_INDEX]
                        * VendorConstants.RATING_EMERGENCY[2];
                lAverageWeight += VendorConstants.RATING_EMERGENCY[2];
            }
            // 4. Experience
            if (this.experience < 3) {
                lWeightedRating += VendorConstants.PARAM_WEIGHT_EMERGENCY[VendorConstants.EXPERIENCE_INDEX]
                                * VendorConstants.EXPERIENCE_EMERGENCY[0];
                lAverageWeight += VendorConstants.EXPERIENCE_EMERGENCY[0];
            } else if (this.experience >= 3 && this.experience < 5) {
                lWeightedRating += VendorConstants.PARAM_WEIGHT_EMERGENCY[VendorConstants.EXPERIENCE_INDEX]
                        * VendorConstants.EXPERIENCE_EMERGENCY[1];
                lAverageWeight += VendorConstants.EXPERIENCE_EMERGENCY[1];
            } else {
                lWeightedRating += VendorConstants.PARAM_WEIGHT_EMERGENCY[VendorConstants.EXPERIENCE_INDEX]
                        * VendorConstants.EXPERIENCE_EMERGENCY[2];
                lAverageWeight += VendorConstants.EXPERIENCE_EMERGENCY[2];
            }
        } else {
            // 1. Compute Job Fee difference
            //Fee Difference is currently decided to be the End range. 
            double feeDiff = this.feeEndRange;
            if (feeDiff < 200) {
                lWeightedRating += VendorConstants.PARAM_WEIGHT[VendorConstants.JOB_FEE_INDEX]
                                * VendorConstants.JOB_FEE[0];
                lAverageWeight += VendorConstants.JOB_FEE[0];
            } else if (feeDiff >= 200 && feeDiff < 500) {
                lWeightedRating += VendorConstants.PARAM_WEIGHT[VendorConstants.JOB_FEE_INDEX]
                        * VendorConstants.JOB_FEE[1];
                lAverageWeight += VendorConstants.JOB_FEE[1];
            } else {
                lWeightedRating += VendorConstants.PARAM_WEIGHT[VendorConstants.JOB_FEE_INDEX]
                        * VendorConstants.JOB_FEE[2];
                lAverageWeight += VendorConstants.JOB_FEE[2];
            }
            // 2. Proximity
            if (this.proximity < 15) {
                lWeightedRating += VendorConstants.PARAM_WEIGHT[VendorConstants.PROXIMITY_INDEX]
                                * VendorConstants.PROXIMITY[0];
                lAverageWeight += VendorConstants.PROXIMITY[0];
            } else if (this.proximity >= 15 && this.proximity < 30) {
                lWeightedRating += VendorConstants.PARAM_WEIGHT[VendorConstants.PROXIMITY_INDEX]
                        * VendorConstants.PROXIMITY[1];
                lAverageWeight += VendorConstants.PROXIMITY[1];
            } else {
                lWeightedRating += VendorConstants.PARAM_WEIGHT[VendorConstants.PROXIMITY_INDEX]
                        * VendorConstants.PROXIMITY[2];
                lAverageWeight += VendorConstants.PROXIMITY[2];
            }
            // 3. Rating
            if (this.rating <= 2) {
                lWeightedRating += VendorConstants.PARAM_WEIGHT[VendorConstants.RATING_INDEX]
                                * VendorConstants.RATING[0];
                lAverageWeight += VendorConstants.RATING[0];
            } else if (this.rating > 2 && this.rating < 4) {
                lWeightedRating += VendorConstants.PARAM_WEIGHT[VendorConstants.RATING_INDEX]
                        * VendorConstants.RATING[1];
                lAverageWeight += VendorConstants.RATING[1];
            } else {
                lWeightedRating += VendorConstants.PARAM_WEIGHT[VendorConstants.RATING_INDEX]
                        * VendorConstants.RATING[2];
                lAverageWeight += VendorConstants.RATING[2];
            }
            // 4. Experience
            if (this.experience < 3) {
                lWeightedRating += VendorConstants.PARAM_WEIGHT[VendorConstants.EXPERIENCE_INDEX]
                                * VendorConstants.EXPERIENCE[0];
                lAverageWeight += VendorConstants.EXPERIENCE[0];
            } else if (this.experience >= 3 && this.experience < 5) {
                lWeightedRating += VendorConstants.PARAM_WEIGHT[VendorConstants.EXPERIENCE_INDEX]
                        * VendorConstants.EXPERIENCE[1];
                lAverageWeight += VendorConstants.EXPERIENCE[1];
            } else {
                lWeightedRating += VendorConstants.PARAM_WEIGHT[VendorConstants.EXPERIENCE_INDEX]
                        * VendorConstants.EXPERIENCE[2];
                lAverageWeight += VendorConstants.EXPERIENCE[2];
            }
        }
        this.setWeightedRating(lWeightedRating);
        this.setAverageWeight(lAverageWeight);
    }

    @Override
    public int compareTo(Vendor v) {
        if (null == v) return -1;
        return (this.mWeightedRating > v.mWeightedRating) ? -1
            : ((this.mWeightedRating < v.mWeightedRating) ? 1
            : ((this.mAverageWeight > v.mAverageWeight) ? -1
            : ((this.mAverageWeight < v.mAverageWeight) ? 1
            : (this.mRequestTime < v.mRequestTime ? -1 : 0))));
    }

    private final boolean printWeight = true;
    private final boolean printEnabled = false;

    public String toString() {
        String str = "{ id : "+this.vendorId
                     +", exp : "+this.experience
                     +", rating : "+this.rating
                     +", fee : "+this.feeStartRange+"-"+this.feeEndRange
                     +", proximity : "+this.proximity
                     +", Req Time : "+this.mRequestTime+" }";

        String strWeight = "{ id : "+this.vendorId
                     +", WR : "+this.mWeightedRating
                     +", AR  : "+this.mAverageWeight
                     +", Time : "+this.mRequestTime+" }";
        return printWeight ? strWeight : str;
    }

    public void print() {
        if (printEnabled) {
            System.out.println(this.toString());
        }
    }
}
