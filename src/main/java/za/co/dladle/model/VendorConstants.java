package za.co.dladle.model;

public class VendorConstants {
    /**
     * 
     * 
     */

    public static final int JOB_FEE_INDEX = 0;
    public static final int PROXIMITY_INDEX = 1;
    public static final int RATING_INDEX = 2;
    public static final int EXPERIENCE_INDEX = 3;

    public static final double[] PARAM_WEIGHT = {45.0, 30.0, 15.0, 10.0};
    public static final double[] PARAM_WEIGHT_EMERGENCY = {30.0, 50.0, 15.0, 5.0};

    // Parameter-1 : JOB FEE [Less than 200, 200-500, 500 and more]
    public static final double[] JOB_FEE = {0.5, 0.3, 0.2};
    public static final double[] JOB_FEE_EMERGENCY = {0.5, 0.3, 0.2};

    // Parameter-2 : Proximity [Less than 15 minutes, 15-30 minutes, 30 minutes and more]
    public static final double[] PROXIMITY = {0.5, 0.3, 0.2};
    public static final double[] PROXIMITY_EMERGENCY = {0.6, 0.3, 0.1};

    // Parameter-3 : Vendor Rating [1-2 stars, 3 stars, 4-5 stars]
    public static final double[] RATING = {0.2, 0.3, 0.5};
    public static final double[] RATING_EMERGENCY = {0.1, 0.2, 0.7};

    // Parameter-4 : Years of Experience [Less than 3 years, 3-5 years, 5 years and more]
    public static final double[] EXPERIENCE = {0.2, 0.3, 0.5};
    public static final double[] EXPERIENCE_EMERGENCY = {0.2, 0.3, 0.5};

    /** 
    * 1. The number of values in non emergency and emergency must be the same.
    * 2. New parameters introduced must be included here.
    */
    static {
        assert JOB_FEE.length == JOB_FEE_EMERGENCY.length;
        assert PROXIMITY.length == PROXIMITY_EMERGENCY.length;
        assert RATING.length == RATING_EMERGENCY.length;
        assert EXPERIENCE.length == EXPERIENCE_EMERGENCY.length;
    }
}
